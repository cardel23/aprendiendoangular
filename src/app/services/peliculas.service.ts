// Servicio para conectar a backend node
import { Injectable } from '@angular/core';
import { Pelicula } from '../models/pelicula';

@Injectable()
export class PeliculaService{

    public peliculas: Pelicula[];

    constructor(){
        this.peliculas = [
            new Pelicula("Fantasma en la concha", 2017, "https://cdn2.actitudfem.com/media/files/styles/big_img/public/images/2017/03/ghost.jpg"),
            new Pelicula("Los Avengers", 2018, "https://bombanoise.com/wp-content/uploads/2019/02/avengers-endgame-heroes-in-war-wallpaper-800x600-15969_17-1.jpg"),
            new Pelicula("El Bromas", 2019, "https://images.wallpapersden.com/image/download/2019-joker-movie-4k_66970_800x600.jpg"),
            // {year:2017, title: "Fantasma en la Concha", image: "https://cdn2.actitudfem.com/media/files/styles/big_img/public/images/2017/03/ghost.jpg"},
            // {year:2019, title: "Los Avengers", image: "https://bombanoise.com/wp-content/uploads/2019/02/avengers-endgame-heroes-in-war-wallpaper-800x600-15969_17-1.jpg"},
            // {year:2019, title: "El Bromas", image:"https://images.wallpapersden.com/image/download/2019-joker-movie-4k_66970_800x600.jpg"}
          ];
    }

    olakAse(){
        return 'Ola k ase desde un servicio de Angular';
    }

    getPeliculas(){
        return this.peliculas;
    }
}