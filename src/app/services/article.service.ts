import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from "src/app/models/article";

import { GlobalConfig } from "./global";

@Injectable()
export class ArticleService {

    public url: string;

    constructor(
        private _http: HttpClient

    ) {
        this.url = GlobalConfig.url;
    }

    pruebas() {
        return "Servicio de articulos";
    }

    // hay que especificar observable
    // parametro last opcional
    getArticles(last: any = null): Observable<any> {

        var articles = 'articles';

        if (last != null) {
            articles = 'articles/true';
        }

        return this._http.get(this.url + articles);
    }

    getArticle(id): Observable<any> {
        return this._http.get(this.url + 'article/' + id);
    }

    search(string): Observable<any> {
        return this._http.get(this.url + 'search/' + string);
    }

    create(article): Observable<any> {
        var params = JSON.stringify(article);
        var headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.post(this.url + 'save', params, { headers: headers });
    }

    update(id, article): Observable<any> {
        var params = JSON.stringify(article);
        var headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.put(this.url + 'article/' + id, params, { headers: headers });
    }

    delete(id): Observable<any> {
        var headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.delete(this.url + 'article/' + id, { headers: headers });
    }


}