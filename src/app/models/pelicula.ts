/**
 * Entidad pelicula
 */

export class Pelicula {
    // public title: string;
    // public image: string;
    // public year: number;

    // constructor(title, year, image){
    //     this.title = title;
    //     this.image = image;
    //     this.year = year;
    // }

    constructor(
        public title: string,
        public year: number,
        public image: string,
    ){};
}