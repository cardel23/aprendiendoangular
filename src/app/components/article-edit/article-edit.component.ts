import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GlobalConfig } from 'src/app/services/global';
import swal from 'sweetalert';

@Component({
  selector: 'app-article-new',
  templateUrl: '../article-new/article-new.component.html',
  providers: [ArticleService],
  styleUrls: ['./article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {

  public article: Article;
  public status: string;
  public isEdit: boolean;
  public pageTitle: string;
  public url: string;

  constructor(
    private _articleService: ArticleService,
    private _router: Router,
    private _route: ActivatedRoute

  ) {
    this.article = new Article('', '', '', null, null);
    this.isEdit = true;
    this.pageTitle = "Editar artículo";
    this.url = GlobalConfig.url;
  }

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "50",
    uploadAPI: {
      url: GlobalConfig.url + 'upload-image/',
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Suba la imagen del articulo...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
    // attachPinText: ""
  };

  ngOnInit(): void {
    this.getArticle();
  }

  onSubmit() {
    this._articleService.update(this.article._id, this.article).subscribe(
      response => {
        if (response.status == "success") {
          this.status = response.status;
          this.article = response.article;

          swal(
            'Articulo editado',
            "ola k ase",
            "success"
          );

          this._router.navigate(['/blog/articulo', this.article._id]);
        } else {
          this.status = "error";
        }
      },
      error => {
        this.status = "error";
        swal(
          'No se pudo editar',
          ":(",
          "error"
        );
      }
    );
  }

  imgUpload(data) {
    var image_data = JSON.parse(data.response);
    this.article.image = image_data.image;
  }

  getArticle() {
    this._route.params.subscribe(params => {
      let id = params['id'];

      this._articleService.getArticle(id).subscribe(
        response => {
          if (response.article) {
            this.article = response.article;
          } else {
            this._router.navigate(['/home']);
          }
        },
        error => {
          this._router.navigate(['/home']);
        }
      );
    });
  }

}
