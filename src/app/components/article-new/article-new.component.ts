import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GlobalConfig } from 'src/app/services/global';

import swal from 'sweetalert';

@Component({
  selector: 'app-article-new',
  templateUrl: './article-new.component.html',
  providers: [ArticleService],
  styleUrls: ['./article-new.component.css']
})
export class ArticleNewComponent implements OnInit {

  public article: Article;
  public status: string;
  public pageTitle: string;
  public isEdit: boolean;
  public url: string;

  constructor(
    private _articleService: ArticleService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    this.article = new Article('', '', '', null, null);
    this.pageTitle = "Crear artículo";
    this.isEdit = false;
    this.url = GlobalConfig.url;
  }

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "50",
    uploadAPI: {
      url: GlobalConfig.url + 'upload-image/',
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Suba la imagen del articulo...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !'
    }
    // attachPinText: ""
  };

  ngOnInit(): void {
  }

  onSubmit() {
    this._articleService.create(this.article).subscribe(
      response => {
        if (response.status == "success") {
          this.status = response.status;
          this.article = response.article;

          swal(
            'Articulo creado',
            "ola k ase",
            "success"
          );

          this._router.navigate(['/blog']);
          console.log(response);
        } else {
          this.status = "error";
        }
      },
      error => {
        this.status = "error";
      }
    );
  }

  imgUpload(data) {
    var image_data = JSON.parse(data.response);
    this.article.image = image_data.image;
  }

}
