import { Component, OnInit } from '@angular/core';

import { ArticleService } from "../../services/article.service";
import { Article } from 'src/app/models/article';
import { GlobalConfig } from 'src/app/services/global';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  providers: [ArticleService],
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  public articles: Article[];
  public url: string;

  constructor(private _articleService: ArticleService) {
    this.url = GlobalConfig.url;
   }

  ngOnInit(): void {
    // console.log(this._articleService.pruebas());

    /**
     * Subscribe usa 2 funciones de callback (response, error)
     */
    this._articleService.getArticles().subscribe(response =>{
      if(response.articles){
        this.articles = response.articles;
      }
    },
    error => {
      console.log(error);
    });
  }

}
