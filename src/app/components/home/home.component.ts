import { Component, OnInit } from '@angular/core';

import { ArticleService } from "../../services/article.service";
import { Article } from 'src/app/models/article';
import { GlobalConfig } from 'src/app/services/global';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ArticleService],
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public articles: Article[];
  public url: string;
  public title: string;

  constructor(private _articleService: ArticleService) {
    this.url = GlobalConfig.url;
    this.title = "Últimos artículos";
  }

  ngOnInit(): void {
    // console.log(this._articleService.pruebas());

    /**
     * Subscribe usa 2 funciones de callback (response, error)
     */
    this._articleService.getArticles(true).subscribe(response =>{
      if(response.articles){
        this.articles = response.articles;
      }
    },
    error => {
      console.log(error);
    });
  }

}
