import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public user: any;
  public campo: string;
  constructor() {
    this.user = {
      nombre: "",
      apellido: "",
      bio: "",
      genero: ""
    };
   }

  ngOnInit(): void {
  }

  /**
   * Evento de submit
   */
  onSubmit(){
    // alert("Ola k ase");
    console.log(this.user);
  }

  clic(){
    alert("Ola k ase");
  }

  salir(){
    alert("Salio o k ase");
  }

}
