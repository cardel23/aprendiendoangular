import {Component} from '@angular/core'

// NO PONER ;
@Component({
    // etiqueta <componente></componente>
    selector: "mi-componente",
    // HTML con `` o plantilla
    // template: `
    //     <h1>{{titulo}}</h1>
    //     <p>{{comentario}}</p>
    //     <p>{{year}}</p>
    // `
    templateUrl: './mi-componente.component.html'
})

export class MiComponente{

    public titulo: string;
    public comentario: string;
    public year: number;
    public mostrarPeliculas: boolean;
    public botonPeliculasLabel: string;
    constructor(){
        this.titulo = "Ola k ase soy mi componente";
        this.comentario = "Este es mi primer componente o k ase";
        this.year = 2020;
        this.mostrarPeliculas = true;
        this.botonPeliculasLabel = "Ocultar peliculas";
        console.log(this.titulo, this.comentario, this.year) ;
    }

    ocultarPeliculas(){
        this.mostrarPeliculas = !this.mostrarPeliculas;
        if(!this.mostrarPeliculas)
            this.botonPeliculasLabel = "Mostrar peliculas";
        else
            this.botonPeliculasLabel = "Ocultar peliculas";
    }
}