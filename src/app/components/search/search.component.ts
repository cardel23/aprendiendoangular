import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { ArticleService } from "../../services/article.service";
import { Article } from 'src/app/models/article';
import { GlobalConfig } from 'src/app/services/global';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  providers: [ArticleService],
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public articles: Article[];
  public url: string;
  public search: string;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _articleService: ArticleService
  ) { }

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      var search = params['search'];
      this.search = search;
      console.log(search);

      this._articleService.search(search).subscribe(
        response => {
          if (response.articles) {
            this.articles = response.articles;
          } else {
            this.articles = [];
          }
          console.log(response);
        },
        error => {
          // this._router.navigate(['/home']);
          this.articles = [];
        }
      );
    });
  }
}
