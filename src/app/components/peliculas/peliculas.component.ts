import { Component, OnInit, /* DoCheck para validar*/DoCheck, OnDestroy } from '@angular/core';

// importar pelicula
import { Pelicula } from "../../models/pelicula";

// importar servicio
import { PeliculaService } from 'src/app/services/peliculas.service';

@Component({
  selector: 'peliculas',
  templateUrl: './peliculas.component.html',
  providers: [PeliculaService],
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {

  public titulo: String;
  public peliculas: Array<Pelicula>;
  public favorita: Pelicula;
  public fecha: any;

  // se le pone _ a las propiedades relacionadas con servicios
  constructor(private _peliculaService: PeliculaService) {
    
    this.titulo = "Componente pelicula";
    
    this.peliculas = this._peliculaService.getPeliculas();
    this.fecha = new Date(2020, 10, 31);
  }

  ngOnInit(): void {
    console.log("Componente iniciado");
    console.log(this._peliculaService.olakAse());
  }

  // 
  ngDoCheck() {
    console.log("DoCheck");
  }

  cambiarTitulo() {
    this.titulo = "El titulo ha sido cambiado";
  }

  ngOnDestroy() {
    console.log("El componente se va a eliminar");
  }

  mostrarFavorita(event) {
    console.log(event);
    this.favorita = event.pelicula;
  }

}
